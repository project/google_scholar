Google Scholar
==============

This module allows nodes to output meta tags for Google Scholar. See
https://scholar.google.no/intl/en/scholar/inclusion.html for more details on
these tags.

## Setup

Any node type may be configured to output tags. In the node type admin form,
enable Google Scholar integration, and select which fields should be used for
the each tag. The title, author, and publication tags are required.

Date and file fields will automatically be output in the correct format.

For other fields, you can use the 'Google Scholar' view mode to configure the
output to your requirements.
